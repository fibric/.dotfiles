#!/usr/bin/env fish

echo linking gnupg
set -x source_file ~/Library/Mobile\ Documents/com~apple~CloudDocs/.gnupg
set -x target_file ~/.gnupg
set -x link_type -fs
set -x trigger_download ~/Library/Mobile\ Documents/com~apple~CloudDocs/.gnupg
