#!/usr/bin/env fish

echo linking starship
set -x source_file ~/.dotfiles/config/starship.toml
set -x target_file ~/.config/starship.toml
set -x link_type -fs
