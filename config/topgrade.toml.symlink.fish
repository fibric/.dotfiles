#!/usr/bin/env fish

echo linking topgrade
set -x source_file ~/.dotfiles/config/topgrade.toml
set -x target_file ~/.config/topgrade.toml
set -x link_type -fs
