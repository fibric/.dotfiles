#!/usr/bin/env fish

echo linking git config
set -x source_file ~/Library/Mobile\ Documents/com~apple~CloudDocs/.secrets/gitconfig
set -x target_file ~/.gitconfig
set -x link_type -fs
set -x download ~/Library/Mobile\ Documents/com~apple~CloudDocs/.secrets
