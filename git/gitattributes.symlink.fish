#!/usr/bin/env fish

echo linking git attributes
set -x source_file ~/.dotfiles/git/gitattributes
set -x target_file ~/.gitattributes
set -x link_type -fs
