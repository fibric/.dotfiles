#!/usr/bin/env fish

echo linking git ignore
set -x source_file ~/.dotfiles/git/gitignore
set -x target_file ~/.gitignore
set -x link_type -fs
