#!/usr/bin/env fish

#
if test -z (command git config --global --get user.name)
    # git name
    echo "enter your full name for git: "
    git config --global user.name (read)
end

if test -z (command git config --global --get user.email)
    # git name
    echo "enter your email for git: "
    git config --global user.email (read)
end
