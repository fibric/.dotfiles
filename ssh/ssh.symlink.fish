#!/usr/bin/env fish

echo linking ssh
set -x source_file ~/Library/Mobile\ Documents/com~apple~CloudDocs/.ssh
set -x target_file ~/.ssh
set -x link_type -fs
set -x trigger_download ~/Library/Mobile\ Documents/com~apple~CloudDocs/.ssh
