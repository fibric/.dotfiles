#!/bin/sh

# xcode
install_xcode() {
    if [[ ! -d "/Applications/Xcode.app" ]]; then
        echo "installing xcode"
        open "macappstores://itunes.apple.com/en/app/xcode/id497799835"
        until [[ -d "/Applications/Xcode.app" ]]; do
            echo -n "."
            sleep 15s
        done
        echo ""
        open -a Xcode
    else
        echo "skipping xcode"
    fi
}

# are_xcode_command_line_tools_install() {
#     xcode-select --print-path &> /dev/null
# }

# install_xcode_command_line_tools() {
#     if [[ ! are_xcode_command_line_tools_installed ]]; then
#         echo "installing xcode command line tools"
#         sudo xcode-select --install
#         until $(xcode-select --print-path &> /dev/null); do
#             echo -n "."
#             sleep 15s
#         done
#         echo ""
#     else
#         echo "skipping xcode command line tools"
#     fi
# }

set_xcode_developer_directory() {
    sudo xcode-select -switch "/Applications/Xcode.app/Contents/Developer"
}

agree_with_xcode_licence() {
    sudo xcodebuild -license accept
}

install_xcode_and_xcode_command_line_tools() {
    # install_xcode_command_line_tools
    install_xcode
    set_xcode_developer_directory
    agree_with_xcode_licence
}

install_xcode_and_xcode_command_line_tools

# install brew
# https://brew.sh/
[[ ! -f $(which brew) ]] && /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# install topgrade
[[ ! -f $(which topgrade) ]] && brew install topgrade
# upgrade brew, cask, osx via topgrade
# https://github.com/r-darwish/topgrade
topgrade

# install fish shell
# https://fishshell.com
[[ ! -f $(which fish) ]] && brew install fish

# make fish the default shell
# old way of setting 3rd-party shell as default
    # echo `which fish` | sudo tee -a /etc/shells
    # chsh -s `which fish`
[[ $(dscl . -read $USER/ UserShell | sed 's/UserShell: //') != $(which fish) ]] && sudo dscl localhost -change /Local/Default/Users/$USER UserShell $SHELL $(which fish)

# install iterm2 console app
# https://www.iterm2.com
[[ ! -d $(which /Applications/iTerm.app) ]] && brew install iterm2 --cask
brctl download ~/Library/Mobile\ Documents/com~apple~CloudDocs/iTerm2

# install starship prompt
# https://starship.rs
[[ ! -f $(which starship) ]] && brew install starship

# clone my .dotfiles repo
[[ ! -d $HOME/.dotfiles ]] && git clone https://gitlab.com/fibric/.dotfiles.git $HOME/.dotfiles
[[ -d $HOME/.dotfiles ]] && pushd $HOME/.dotfiles && git pull origin master --no-rebase && popd
[[ ! -x $HOME/.dotfiles/finish.fish ]] && chmod +x $HOME/.dotfiles/finish.fish

# close Terminal.app and switch to iTerm2
# old way of terminating apps
    # osascript -e 'tell application "Terminal" to close first window' & exit
open -a iTerm "$HOME/.dotfiles" && killall Terminal
