# fibric's .dotfiles

`dotfiles` are how you personalize your system. these are mine.

## what's inside

there are a few special files in the hierarchy.

-   **topic/setup.fish**: any file named `setup.fish` is executed when you run `finish-install.fish`.
-   **topic/\*.symlink.fish**: any file ending in `*.symlink.fish` is executed to symlink into `$home`.

## installation

### easy way

run:

```
curl -fsSL --compressed https://gitlab.com/fibric/.dotfiles/-/raw/master/install.sh | zsh
```

### manual way
run:

```sh
git clone https://gitlab.com/fibric/dotfiles.git ~/.dotfiles
chmod +x ~/.dotfiles/install.sh
sh ~/.dotfiles/install.sh
```

`install.sh` installs `brew`, `topgrade`, `fish`, `iterm2`. sets `fish` as default shell, terminates `terminal.app` and opens `iterm2` in `~/.dotfiles` folder.

### finish the installation:

### easy way

run:

```sh
./finish.fish
```

### in cases...

run:

```sh
fish ~/.dotfiles/finsih.fish
```

`finish.fish` continuous and it will - find all `*.symlink.fish` files and executes them to `$home`. - find all `setup.fish` files and executes them.

additionally, `git`, `nano`, and `ruby`, `caddy`, `mkcert`, `brave`, `firefox`, and the awesome victor mono font are installed. `finish-install.fish` lists everything which is gonna be installed via `brew` or `brew cask`.

the next step is to apply all `fish` related changes via sourcing `config.fish` which itself only sources `startup.fish`. i choose this structure to be able to quickly make changes inside of my `.dotfiles` project which then immediately take effect with the next ttr start. other dotfiles project have a somewhat complicated sync procedure.

### caveat

`iterm2` preferences are synced via icloud. ensure the content of the icloud folder `iterm2` is available offline.

## engineering

i switched to a `parallels` based development environment, which is fine for the most parts. that is why not many files are of high interest to sync them across different mac devices or restore them after a fresh installation of macos. yet i have enough files worth syncing. i use icloud for some files because i trust apple more than microsoft or google.

## roadmap

i found this website a while ago [macos setup guide](https://sourabhbajaj.com/mac-setup/) and thought "i can automate this" but then i figured a `parallels` based development environment. the need to automate macos installations has become less important. it may change if i stop paying for `parallels` subscription or can't figure how to easily and effortlessly back up my `parallels` volumes.

### secrets

    - `ssh` keys
    - certificates
    - gpg keys

~~i host my secrets in a private `secrets` reposity. the `finish-install.fish` script will fetch the reposity and applies the secrets. if i figure how to force finder.app to download files from icloud before accessing them i might even wipe my private `.secrets` repository.~~

i host my secrets on icloud and symlinked all files containing secrets. additionally i added them to `.gitignore` to avoid accidentially pushing them.

**attention**: _keep in mind to not include terminal session secrets in `fish/config/fish/startup/variables.fish` or any other file in the `.dotfiles` project._

syncing high valuable secrets is a pain right now. i follow the directive `trust nobody` that's why i haven't automated the process yet. some secrets are easy replacedable (less important to sync but highly important to protect until replaced) others aren't so much. i have a multi factor authentication which i can't automate for syncing yet.
