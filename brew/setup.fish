# helper functions

# install only if the desired command doesn't exist yet
function _should_install_command
    if test -z $argv[2]
        if not command -q $argv[1] 2>/dev/null; command brew install $argv[1] --cask; end
    else
        if not command -q $argv[1] 2>/dev/null; command brew install $argv[2] --cask; end
    end
end

# install only if the desired cask doesn't exist yet
function _should_install_cask
    if test -z $argv[2]
        if not command -q (brew ls --cask $argv[1] 2>/dev/null); command brew install $argv[1] --cask; end
    else
        if not command -q (brew ls --cask $argv[1] 2>/dev/null); command brew install $argv[2] --cask; end
    end
end

# install only if the desired formula doesn't exist yet
# use this to check for commands already provided by macos
# e.g. git, ruby, curl, etc
function _should_install_formula
    if not command -q (brew ls $argv[1] 2>/dev/null); command brew install $argv[1]; end
end

# install updated versions of macOS binaries
_should_install_formula coreutils
_should_install_formula moreutils
_should_install_formula findutils
_should_install_formula gnu-sed
_should_install_formula nano
_should_install_formula git
_should_install_formula ruby
_should_install_formula openssh
_should_install_formula grep

# fonts
_should_install_cask homebrew/cask-fonts/font-victor-mono

# browsers
_should_install_cask homebrew/cask-versions/firefox-developer-edition
_should_install_cask brave-browser
_should_install_cask opera-gx

# package manmagers

# asdf
_should_install_formula asdf

# install asdf plugins
command asdf plugin-add nodejs
command asdf plugin-add crystal
command asdf plugin-add erlang
command asdf plugin-add elixir
command asdf plugin-add deno
command asdf plugin-add yarn

# install favored languages

## nodejs
_should_install_formula gawk
asdf_install nodejs latest
asdf_set_global nodejs (command asdf latest nodejs)
asdf_install yarn latest
asdf_set_global yarn (command asdf latest yarn)

## crystal
asdf_install crystal latest
asdf_set_global crystal (command asdf latest crystal)

## erlang / elixir
_should_install_formula wxmac # wxwidgets
asdf_install erlang latest
asdf_set_global erlang (command asdf latest erlang)
asdf_install elixir latest
asdf_set_global elixir (command asdf latest elixir)

## deno
asdf_install deno latest
asdf_set_global deno (command asdf latest deno)

# rust
_should_install_formula rustup rustup-init

# nim
# choosenim's dependencies
_should_install_formula openssl
_should_install_formula curl
_should_install_formula wget

# choosenim
if not command -sq choosenim; command curl https://nim-lang.org/choosenim/init.sh -sSf | sh; end
# install stable nim
command choosenim stable

# pony
# pony's dependencies
_should_install_formula libressl
# ponyup
if not command -sq ponyup; command curl --proto '=https' --tlsv1.2 -sSf https://raw.githubusercontent.com/ponylang/ponyup/latest-release/ponyup-init.sh | sh; end
# install pony compiler
command ponyup update ponyc release
# install pony deps manager
command ponyup update corral release

# quicklook plugins
_should_install_cask suspicious-package
_should_install_cask qlstephen
_should_install_cask qlmarkdown
# restat quicklook
command qlmanage -r

# others
_should_install_formula mas
_should_install_formula gpg-suite-no-mail
# _should_install_formula caddy
# _should_install_formula mkcert
_should_install_formula exa
_should_install_cask sublime-merge
_should_install_cask sublime-text
_should_install_cask xmind-zen
_should_install_cask parallels
# _should_install_cask parallels-toolbox
_should_install_cask kaleidoscope
_should_install_cask gitkraken
_should_install_cask fork
_should_install_cask visual-studio-code
_should_install_cask grammarly
_should_install_cask deepl
_should_install_cask google-drive-file-stream
_should_install_cask signal
_should_install_cask figma
_should_install_cask nova
# _should_install_cask protonvpn
_should_install_cask discord
_should_install_cask slack
# _should_install_cask engine-prime
# _should_install_cask tidal
_should_install_cask typora
# _should_install_cask steam
# _should_install_cask epic-games
# _should_install_cask battle-net
# _should_install_cask gog-galaxy
_should_install_cask setapp
_should_install_cask papers3
brctl download ~/Library/Mobile\ Documents/com~apple~CloudDocs/Papers3