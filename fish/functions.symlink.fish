#!/usr/bin/env fish

echo linking fish functions
set -x source_file ~/.dotfiles/fish/config/fish/functions
set -x target_file ~/.config/fish/functions
set -x link_type -fs
