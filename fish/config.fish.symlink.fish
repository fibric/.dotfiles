#!/usr/bin/env fish

echo linking fish config
set -x source_file ~/.dotfiles/fish/config.fish
set -x target_file ~/.config/fish/config.fish
set -x link_type -fs
