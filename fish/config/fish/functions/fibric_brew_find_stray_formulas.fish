#!/usr/bin/env fish

function fibric_brew_find_stray_formulas --description 'brew_find stray formulas'
    set -l desc (functions --details -v fibric_brew_find_stray_form)
    function _find_stray_formulas -e find_stray_formulas
        set -l visited_formulas 'brew-cask'
        set -l leaves (command brew leaves)
        for formula in $argv
            set -l dependees (brew uses --installed $formula)
            if [ -z "$dependees" ]
                and \
                    not contains $formula $visited_formulas
                and \
                    not contains $formula $leaves
                read -p "echo \"$formula is not depended on by other formulas - remove it? [y/n] \"" -l input
                set visited_formulas $visited_formulas $formula
                if [ "$input" = "y" ]
                    command brew remove $formula
                    emit check_formulas (brew deps --1 --installed $formula)
                end
            end
        end
    end
    echo start $desc[-1]
    emit find_stray_formulas
    functions -e _find_stray_formulas
    echo done $desc[-1]
end
