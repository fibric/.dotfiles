#!/usr/bin/env fish

function fibric_brew_find_abandoned_casks --description 'brew_find abandoned casks'
    set -l desc (functions --details -v fibric_brew_find_abandoned_casks)
    function _find_abandoned_casks -e find_abandoned_casks
        if command -sq brew cask
            set -l brew_casks (command brew cask ls -1 --versions)
            set -l logging
            set -l found_count 0
            if test "$argv[1]" = '--verbose'; or test "$argv[1]" = '-v'
                set logging 'verbose'
            end
            for cask in $brew_casks
                if test "$logging" = 'verbose'
                    echo checking "$cask"
                end
            end
            set -l includes_from (command brew cask info "$cask" | command grep "From:")
            if test -z "$includes_from"
                echo "$cask" is abandoned! try find an alternative cask
                set found_count (math $found_count + 1)
            end
        end
        if test $found_count = 0
            echo no abandoned casks found
        else
            echo found $found_count abandoned casks
        end
    end
    echo start $desc[-1]
    emit find_abandoned_casks
    functions -e _find_abandoned_casks
    echo done $desc[-1]
end
