#!/usr/bin/env fish

function fibric_topgrade_greedy -d "topgrade_greedy enables the brew cask greedy option once"
    set -l desc (functions --details -v fibric_topgrade_greedy)
    set -x source_file ~/.dotfiles/config/topgrade.toml
    set -x greedy_file ~/.dotfiles/config/topgrade-greedy.toml
    set -x target_file ~/.config/topgrade.toml
    set -x link_type -f
    function _topgrade_greedy -e topgrade_greedy
        if test -e "$target_file"
            command rm -f "$target_file"
            command ln "$link_type" "$greedy_file" "$target_file"
            command topgrade
        end
        command rm -f $target_file
        command ln "$link_type" "$source_file" "$target_file"
    end
    echo start $desc[-1]
    emit topgrade_greedy
    functions -e _topgrade_greedy
    echo done $desc[-1]
end
