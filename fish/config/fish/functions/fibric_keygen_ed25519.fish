#!/usr/bin/env fish

function fibric_keygen_ed25519 -d "keygen_generate ed25519 key"
    set -l desc (functions --details -v fibric_keygen_ed25519)

    # usage
    # keygen_ed25519 file comment
    # keygen_ed25519 file
    # keygen_ed25519

    echo ED25519 it’s the most recommended public-key algorithm available today!

    function _generate_rsa_key -e generate_rsa_key
        echo 1 $argv[1]
        echo 2 $argv[2]
        set -l file
        if [ -n "$argv[1]" ]
            set file ~/.ssh/id_ed25519_"$argv[1]"
        else
            set file ~/.ssh/id_ed25519
        end
        echo generating $file and $file.pub
        # -t algorithm [rsa|dsa|ecdsa|ed25519]
        # -a cycles used before validating signatures
        # -b key size
        # -f output folder
        # -n new passphrase
        # -c comment
        command ssh-keygen -o -a 276 -t ed25519 -C "$argv[2]" -f $file
        command ssh-add -k $file
    end
    echo start $desc[-1]
    emit generate_rsa_key $argv
    functions -e generate_rsa_key
    echo done $desc[-1]
end
