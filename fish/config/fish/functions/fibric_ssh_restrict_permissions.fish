#!/usr/bin/env fish

function fibric_ssh_restrict_permissions -d "ssh_restrict permissions according to gnupg"
    set -l desc (functions --details -v fibric_ssh_restrict_permissions)
    function _restrict_permissions -e restrict_permissions
        command sudo chown -Rh $USER:staff ~/.ssh/
        command sudo chgrp -Rh staff ~/.ssh/
        command sudo chmod -R 700 ~/.ssh/
        command sudo chmod -f 600 ~/.ssh/*
        command sudo chmod -fh 644 ~/.ssh/*.pub ~/.ssh/known_hosts # ~/.ssh/authorized_keys
    end
    echo start $desc[-1]
    emit restrict_permissions
    functions -e _restrict_permissions
    echo done $desc[-1]
end
