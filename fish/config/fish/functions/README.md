# my custom fish functions

i am used to organize my functions by namespace and department.

## status quo

~~when i started to write my first functions in fish, i created a name collision and i thought i had to extend my function names or assign a certain namespace to them.~~

~~namespaces will never end up in fish so i needed a different approach.~~

~~using `::` felt natural because it reminded me of the time when i learned and used the ruby programming language.~~

~~`fibric::keygen::ecdsa.fish`~~

-   ~~`fibric` namespace~~
-   ~~department 'keygen~~
-   ~~`ecdsa' function name~~

~~the clear disadvantage is that fish's `funced' method expects `::`in the filename if you use`::`in the function name. the finder.app shows`fibric//keygen//ecdsa.fish`, while other tools like vscode or iterm show `fibric::keygen::ecdsa` as filename.~~

## ~~possible changes~~

the community seems to have settled with `\_' to establish a soft namespace.

`fibric_keygen_ecdsa.fish`

-   `fibric` namespace
-   `keygen` department
-   `ecdsa` function name

i already use underscores in private functions. i guess i can get used to using underlined file and function names.

## private functions

fish has no concept of private functions. i figured i use function names beginning with an \_ to indicate private use.

the `functions' command would have listed these functions and thus missed the purpose of private functions.

`functions -e <functionname>` deletes a function from the execution environment.

keeping the output of the `functions` command clean.

in the javascript world, developers use higher-order functions, and since i am a javascript developer, i thought i would define my private functions within my higher-order function, call private functions from inside of the higher-order-function, and then delete the private functions.
