#!/usr/bin/env fish

function fibric_editor_download_editorconfig -d "editor_download .edidorconfig"
    set -l desc (functions --details -v fibric_editor_download_editorconfig)
    function _download_editorconfig -e download_editorconfig
        command curl -fsSL https://gitlab.com/-/snippets/20069/raw -m 120 --retry 2 >.editorconfig
    end
    echo start $desc[-1]
    emit download_editorconfig
    functions -e _download_editorconfig
    echo done $desc[-1]
end
