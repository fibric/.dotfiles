#!/usr/bin/env fish

function fibric_keygen_ecdsa -d "keygen_generate ecdsa key"
    set -l desc (functions --details -v fibric_keygen_ecdsa)

    # usage
    # keygen_ecdsa file comment
    # keygen_ecdsa file
    # keygen_ecdsa

    echo beware ECDSA depends on how well your machine can generate a random number that will be used to create a signature
    echo there’s also a trustworthiness concern on the NIST curves that being used by ECDSA and where sponsored by NSA

    function _generate_ecdsa_key -e generate_ecdsa_key
        set -l file
        if [ -n "$argv[1]" ]
            set file ~/.ssh/id_ecdsa_"$argv[1]"
        else
            set file ~/.ssh/id_ecdsa
        end
        echo generating $file and $file.pub
        # -t algorithm [rsa|dsa|ecdsa|ed25519]
        # -a cycles used before validating signatures
        # -b key size
        # -f output folder
        # -n new passphrase
        # -c comment
        command ssh-keygen -o -a 276 -t ecdsa -b 521 -C "$argv[2]" -f $file
        command ssh-add -k $file
    end
    echo start $desc[-1]
    emit generate_ecdsa_key $argv
    functions -e _generate_ecdsa_key
    echo done $desc[-1]
end
