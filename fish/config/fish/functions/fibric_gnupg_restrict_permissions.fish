#!/usr/bin/env fish

function fibric_gnupg_restrict_permissions -d "gnupg_restrict permissions according to gnupg"
    set -l desc (functions --details -v fibric_gnupg_restrict_permissions)
    function _restrict_permissions -e restrict_permissions
        command sudo chown -Rh $USER:staff ~/.gnupg/
        command sudo chgrp -Rh staff ~/.gnupg/
        command sudo chmod -R 700 ~/.gnupg/
        command sudo chmod -f 600 ~/.gnupg/*
    end
    echo start $desc[-1]
    emit restrict_permissions
    functions -e _restrict_permissions
    echo done $desc[-1]
end
