#!/usr/bin/env fish

alias "cd.." "cd .."
# alias nodenv-list-versions 'nodenv install -l | egrep -i "^(?:\s+)?\d{1,}(\.\d{1,}){2,2}(?:\s+)?\$" | sort -V'
# alias nodenv-list-versions-unfiltered 'nodenv install -l'
alias sha256 "shasum -a 256 "
alias sha384 "shasum -a 384 "
alias sha512 "shasum -a 512 "
alias git_clean_untracked "git clean fDx"
# use code for code-insiders if vscode is not installed
if not command -sq code
    if command -sq code-insiders
        alias code code-insiders
    end
end
alias asdf_list_installed "asdf list"
alias asdf_list_available "asdf list all"
alias asdf_current "asdf current"
alias asdf_latest "asdf latest"
alias asdf_install "asdf install"
alias asdf_local "asdf local"
alias asdf_global "asdf global"
alias asdf_set_shell "asdf shell"
alias asdf_uninstall "asdf uninstall"
alias gi_update "git ignore --update"
alias gi_list "git ignore --list"
alias gi_new "git ignore $argv > .gitignore"
alias gi_add "git ignore $argv >> .gitignore"
alias delete_all_ds_store "find $argv -type f -name '.DS_Store' -ls -delete"
alias build_Locate_database "sudo launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist"
alias rebuild_launchservices_database "sudo (locate lsregister) -kill -seed -r"
alias has "curl -sL https://git.io/_has | bash -s"

# replace cached hex and rebar with version required by local project
if command -sq mix
    alias use_mix "mix local.hex"
    alias use_rebar "mix local.rebar"
end

# query dns parameters
if command -sq dog
    alias dns "dog $argv A AAAA CAA CNAME HINFO LOC MX NS PTR SOA SRV TXT Others"
end

# replace ls and add ll
if command -sq exa
    alias ls "exa --all --classify --group-directories-first --git"
    alias ll "exa --all --long --links --classify --group-directories-first --git"
else
    alias ls "ls -AHL"
    alias ll "ls -AHLlF"
end
