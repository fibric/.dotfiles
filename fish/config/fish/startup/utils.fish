#!/usr/bin/env fish

# Open man page as PDF
function man --wraps='man' --description 'Opens man pages as PDF in Preview.app'
    command man -t "$argv[1]" | open -f -a /System/Applications/Preview.app
end
