#!/usr/bin/env fish

# add $HOME/.local/bin to PATH variable
set -gx fish_user_paths $HOME/.local/bin $fish_user_paths

# coreutils
set -gx fish_user_paths (command brew --prefix coreutils)/libexec/gnubin

# asdf
[ -f (brew --prefix asdf)/asdf.fish ] && source (brew --prefix asdf)/asdf.fish

# nimble
set -gx fish_user_paths $HOME/.nimble/bin $fish_user_paths

# ponyup
set -gx fish_user_paths $fish_user_paths $HOME/.local/share/ponyup/bin

# python3.9
set -gx fish_user_paths /usr/local/opt/python/libexec/bin $fish_user_paths
set -gx fish_user_paths /usr/local/opt/python@3.9/bin $fish_user_paths

# ruby
set -gx fish_user_paths "/usr/local/opt/ruby/bin" $fish_user_paths

# rustc
set -gx fish_user_paths $HOME/.cargo/bin $fish_user_paths

# nodenv specific
# set -gx NODE_BUILD_DEFINITIONS (command brew --prefix node-build-update-defs)/share/node-build

# openssl
# set -gx LDFLAGS "-L/usr/local/opt/openssl@1.1/lib" $LDFLAGS
# set -gx CPPFLAGS "-I/usr/local/opt/openssl@1.1/include" $CPPFLAGS
# set -gx PKG_CONFIG_PATH "/usr/local/opt/openssl@1.1/lib/pkgconfig" $PKG_CONFIG_PATH

# libressl
# set -gx LDFLAGS "-L/usr/local/opt/libressl/lib" $LDFLAGS
# set -gx CPPFLAGS "-I/usr/local/opt/libressl/include" $LDFLAGS

# openjdk
# required by fop (required by erlang)
# set -gx CPPFLAGS "-I/usr/local/opt/openjdk/include" $LDFLAGS
# set -gx fish_user_paths "/usr/local/opt/openjdk/bin" $fish_user_paths

# android
# set java home only when either java@13 or java@8 have been found
# set -gx JAVA_HOME (/usr/libexec/java_home -v 1.8 -a x86_64)
# or set -gx JAVA_HOME (/usr/libexec/java_home -v 13 -a x86_64)
# or : # no-op

# if test -d $HOME/Library/Android/sdk
#     set -gx ANDROID_HOME $HOME/Library/Android/sdk
#     set -gx fish_user_paths $ANDROID_HOME/emulator $fish_user_paths
#     set -gx fish_user_paths $ANDROID_HOME/tools $fish_user_paths
#     set -gx fish_user_paths $ANDROID_HOME/tools/bin $fish_user_paths
#     set -gx fish_user_paths $ANDROID_HOME/platform-tools $fish_user_paths
# end
