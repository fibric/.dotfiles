#!/usr/bin/env fish

set -xU KERL_CONFIGURE_OPTIONS --without-javac --with-ssl=(brew --prefix openssl)
set -xU KERL_BUILD_DOCS yes

# editor
function _set_editor
    if command -sq "$argv[1]"
        set -l _path (command which $argv[1])
        set -Ux EDITOR $_path
        set -Ux VISUAL $_path
        set -Ux GIT_EDITOR $_path
        git config --global core.editor $argv[1]
        set -e _path
    end
end

# the editor that is listed most right handed is given highest priority
for editor in nano subl nova
    _set_editor $editor
end

functions -e _set_editor
