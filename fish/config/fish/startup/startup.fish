#!/usr/bin/env fish

source ~/.dotfiles/fish/config/fish/startup/variables.fish
source ~/.dotfiles/fish/config/fish/startup/paths.fish
source ~/.dotfiles/fish/config/fish/startup/aliases.fish
source ~/.dotfiles/fish/config/fish/startup/utils.fish

# if status --is-interactive
# 	and source (command nodenv init - --no-rehash | psub)
# end

abbr -a -- - 'cd -'

if command -sq starship
    # eval (starship init fish) # starship fish package
    starship init fish | source # starship rust package
end
