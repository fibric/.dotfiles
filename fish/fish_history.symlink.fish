#!/usr/bin/env fish

echo linking fish history
set -x source_file ~/Library/Mobile\ Documents/com~apple~CloudDocs/fish_history
set -x target_file ~/.local/share/fish/fish_history
set -x link_type -fs
set -x trigger_download ~/Library/Mobile\ Documents/com~apple~CloudDocs/fish_history
