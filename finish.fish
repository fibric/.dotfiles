#!/usr/bin/env fish

# finds all `.symlink` files and executes them
set -l FILES_TO_SYMLINK (command find $HOME/.dotfiles -name '*.symlink.fish')
for link in $FILES_TO_SYMLINK
    source "$link"
    # $target_file is defined when sourcing $link
    # $source_file is defined when sourcing $link
    # $link_type is defined when sourcing $link
    [ ! $link_type = -f ]; and [ -d $target_file ]; and rm -rf $target_file
    ln $link_type $source_file $target_file
    [ $download ]; brctl download $download
    set -e source_file
    set -e target_file
    set -e link_type
end

# enable fish env
source $HOME/.dotfiles/fish/config.fish

# finds all `setup.fish` files and executes them
set -l SETUP_FILES (command find $HOME/.dotfiles -name 'setup.fish')
for setup in $SETUP_FILES
    source "$setup"
end

#restict file permissions
fibric_ssh_restrict_permissions
fibric_gnupg_restrict_permissions
