#!/usr/bin/env fish

echo Setting up macOS defaults

# Hardware
## Chime When Charging
defaults write com.apple.PowerChime ChimeOnAllHardware -bool true && \
open /System/Library/CoreServices/PowerChime.app

## Disable Sudden Motion Sensor
sudo pmset -a sms 0

# macOS
## Set Software Update Check Interval
defaults write com.apple.SoftwareUpdate ScheduleFrequency -int 1

## Set Menu Bar Clock Output Format
sudo defaults write com.apple.menuextra.clock DateFormat -string "EEE d MMM h:mm a"

# Dock
## Automatically Hide
defaults write com.apple.dock autohide -bool true

## Lock the Dock Size
defaults write com.apple.Dock size-immutable -bool yes

## Scroll Gestures
defaults write com.apple.dock scroll-to-open -bool true

## Show Hidden App Icons
defaults write com.apple.dock showhidden -bool true

## Show Recent Apps
defaults write com.apple.dock show-recents -bool false

sudo killall Dock Safari Finder SystemUIServer
